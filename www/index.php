<?php

/*
 * This file is part of Qerana
 * Copyright (C) 2019-20  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerana;

/**
 * -----------------------------------------------------------------------------
 * is QeranaCli?
 * -----------------------------------------------------------------------------
 */
if (PHP_SAPI == 'cli') {
    define('__ISCLI__', 1);
    // cli is true
    define('__DOCUMENTROOT__', substr(str_replace(pathinfo(__FILE__, PATHINFO_BASENAME), '', __FILE__), 0, -1));
} else {
    // is http petition
    define('__DOCUMENTROOT__', filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . '');
}

// log location
define('__LOG__', __DOCUMENTROOT__ . '/../_data_/logs/logs.log');

// data location
define('__DATA__', __DOCUMENTROOT__ . '/../_data_/');

/**
 * -----------------------------------------------------------------------------
 * check petition, if not over ssl, REDIRECT
 * -----------------------------------------------------------------------------
 */
if (empty(filter_input(INPUT_SERVER, 'HTTPS'))) {
    header('Location: https://' . filter_input(INPUT_SERVER, 'SERVER_NAME'));
}

/**
 * -----------------------------------------------------------------------------
 * SITE URL
 * -----------------------------------------------------------------------------
 */
define('__URL__', 'https://' . filter_input(INPUT_SERVER, 'HTTP_HOST') . '');

/**
 * -----------------------------------------------------------------------------
 * QERANA PROJECT NAME
 * -----------------------------------------------------------------------------
 */
define('__APPNAME__', 'qfw');

/*
 * ---------------------------------------------------------------
 * CORE SYSTEM DIRECTORY
 * ---------------------------------------------------------------
 */
define('__COREFOLDER__', __DOCUMENTROOT__ . '/../qerana/');

/*
 * ---------------------------------------------------------------
 * QERAPPS FOLDERS
 * ---------------------------------------------------------------
 */
define('__QERAPPSFOLDER__', __DOCUMENTROOT__ . '/../qerapps/');
/*
 * ---------------------------------------------------------------
 * QERAPP SUBSYSTEM
 * ---------------------------------------------------------------
 */
define('__QERAPP__', 'Qerapp');

/*
 * ---------------------------------------------------------------
 * ROOT SYSTEM DIRECTORY
 * ---------------------------------------------------------------
 */
define('__ROOTFOLDER__', __DOCUMENTROOT__ . '/../');

//die(__COREFOLDER__);
/*
 * ---------------------------------------------------------------
 * APPLICATION FOLDER
 * ---------------------------------------------------------------
 *    
 */

define('__APP__', 'app');

define('__APPFOLDER__', __DOCUMENTROOT__ . '/../' . __APP__ . '/');



/**
 * -----------------------------------------------------------------------------
 * MODULE FOLDER
 * -----------------------------------------------------------------------------
 */
define('__MODULEFOLDER__', __APPFOLDER__ . '');


/*
 * ---------------------------------------------------------------
 * APP environment
 * ---------------------------------------------------------------
  /**
 * Set the general app environment.
 * if "development" is defined , all errors will show
 * values permited (desarrollo,produccion,testing)
 */
define('__ENVIRONMENT__', 'development');

if (defined('__ENVIRONMENT__')) {
    switch (__ENVIRONMENT__) {
        case 'development':
            //error_reporting(0);
            ini_set('display_errors', 1);
            error_reporting(-1);
            error_reporting(E_ALL);
            break;

        case 'testing':
        case 'production':
            error_reporting(0);
            break;

        default:
            exit('Qerana dosnt have a valid environment, bye');
    }
}

/**
 * ------------------------------------------------------------------------------
 * Create a NEW System Object, AND run the application
 * ------------------------------------------------------------------------------
 * see Vendor/composer.json for autoload settings
 * if you add a new namespace, dont forget to run composer update
 * (    composer dump-autoload --optimize ) in command line, 
 * in the same level of  vender folder
 */
require __DOCUMENTROOT__ . '/../qerana/vendor/autoload.php';


New core\Bootstrap();

