<!-- SIDEBAR INIT -->
<ul class="navbar-nav bg-gray-900 sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo __URL__; ?>">
        <div class="sidebar-brand-icon rotate-n-15 text-warning">
            <i class="fab fa-cloudsmith"></i>
        </div>
        <div class="sidebar-brand-text mx-3 text-warning">qfw</div><sup clas="small"></sup>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="/qdevtools/qdevtools/index">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>ControlPanel</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">


    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Components</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Custom Components:</h6>
                <a class="collapse-item" href="buttons.html">Emails</a>
                <a class="collapse-item" href="cards.html">Emails Accounts</a>
                <div class="collapse-divider"></div>
                <h6 class="collapse-header">Documents:</h6>
            </div>

        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-users"></i>
            <span>Users</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">User Utilities:</h6>
                <a class="collapse-item" href="/qaccess/user/index">Users</a>
                <a class="collapse-item" href="/qaccess/group/index">Groups</a>
                <a class="collapse-item" href="/qaccess/profile/index">Profiles</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- END SIDEBAR-->
