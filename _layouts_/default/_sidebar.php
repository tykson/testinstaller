<!-- SIDEBAR INIT -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo __URL__; ?>">
        <div class="sidebar-brand-icon rotate-n-15 text-warning">
            <i class="fab fa-cloudsmith"></i>
        </div>
        <div class="sidebar-brand-text mx-3 text-gray-500"><?php echo __APPNAME__; ?> <sup>1</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">


    <!-- Divider -->
    <hr class="sidebar-divider">

    <?php
    foreach ($_SESSION['navmap_user'] AS $module => $controllers):
        ?>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" 
               data-target="#collapse_<?php echo $module; ?>" 
               aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-arrow-right"></i>
                <span><?php echo ucfirst($module); ?></span>
            </a>
            <div id="collapse_<?php echo $module; ?>" class="collapse" 
                 aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <?php foreach ($controllers AS $controller => $actions): ?>

                        <a class="collapse-item" href="/<?php echo $module.'/'.$controller.'/index';?>"><?php echo ucfirst($controller); ?></a>
                    <?php endforeach; ?>
                </div>

            </div>
        </li>
    <?php endforeach; ?>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- END SIDEBAR-->
