<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />
        <meta name="lang" content="es" />
        <meta name="author" content="qerana" />
        <meta name="organization" content="qerana" />
        <meta name="locality" content="qworld" />
        <title>:-)<?php echo __APPNAME__;?>-Redirecting!</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script type="text/javascript">
            setTimeout(function () {
                window.location.href = '<?php echo __URL__; ?>';
            }, 1000);
        </script>
    </head>
    <body style="background-color: #F8F8F8;" >
        <div class="container">

            <div class="card mb-4 ">
                <div class="card-header py-3">
                    <h2 class="">
                        <small class="text-success"><?php echo $title; ?></small>
                    </h2>
                </div>
                <div class="card-body py-3">
                    <p>
                        <?php echo $description; ?>
                    </p>
                    <p>
                        Redirigiendo...
                    </p>
                </div>
                <hr>
            </div>

    </body>
</html>
<?php
die();
