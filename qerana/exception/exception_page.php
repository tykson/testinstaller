<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />
        <meta name="lang" content="es" />
        <meta name="author" content="qerana" />
        <meta name="organization" content="qerana" />
        <meta name="locality" content="qworld" />
        <title>Error500</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    </head>
        <body style="background-color: #778899;" >
        <div class="container" align="center1">
            <div class="col-xs-10 col-sm-5 col-md-7">
                <div class="card mb-4 ">
                    <div class="card-header py-3">
                        <h2 class="">
                            <i class="text-danger">Error500</i> 
                            <small class="text-gray">Internal Server Error</small>
                        </h2>
                    </div>
                    <div class="card-body py-3">
                        <h4 class="text-danger"><?php echo $title; ?></h4>
                        <?php if (__ENVIRONMENT__ == 'development') {
                            ?>

                            <h5>File</h5>
                            <p>
                                <?php echo $exception->getFile() . ' <b>( Line:' . $exception->getLine() . ')</b>'; ?>
                            </p>
                            <h5>Message</h5>
                            <p>
                                <?php echo $exception->getMessage(); ?>
                            </p>
                            <h5>Trace</h5>
                            <p>
                                <?php echo nl2br($exception->getTraceAsString()); ?>
                            </p>
                            <?php if (!empty($query)) { ?>
                                <h4>Query</h4>
                                <p><?php echo $query; ?></p>
                                <h4>Query-binds</h4>
                                <p>
                                    <?php print_r($binds); ?>
                                </p>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <hr>
                    <div class="panel-footer " align="center" style="padding: 10px">
                        <button class="btn btn-secondary"  onclick='history.back()'>GoBack</button>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
