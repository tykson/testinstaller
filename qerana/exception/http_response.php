<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />
        <meta name="lang" content="es" />
        <meta name="author" content="qerana" />
        <meta name="organization" content="qerana" />
        <meta name="locality" content="qworld" />
        <title>Error <?php echo $code_status; ?></title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    </head>
    <body style="background-color: #F8F8F8;" >
        <div class="container" align="center1">
            <div class="col-xs-10 col-sm-5 col-md-7">
                <div class="card mb-4 ">
                    <div class="card-header py-3">
                        <h2 class="">
                            <i class="<?php echo $class; ?>">Error <?php echo $code_status; ?></i> 
                            <small class="text-gray"><?php echo $status; ?></small>
                        </h2>
                    </div>
                    <div class="card-body py-3">
                        <p>
                            <?php echo $message; ?>
                        </p>
                    </div>
                    <hr>
                    <div class="panel-footer " align="center" style="padding: 10px">
                        <button class="btn btn-secondary"  onclick='history.back()'>GoBack</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<?php
die();
