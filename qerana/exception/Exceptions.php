<?php

/*
 * This file is part of keranaProject
 * Copyright (C) 2017-2018  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace QException;

use Exception;
use Monolog\Logger,
    Monolog\Handler\StreamHandler,
    Monolog\Handler\FirePHPHandler,
    Monolog\ErrorHandler;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');

class Exceptions extends Exception
{

    /**
     * http status code
     * @var type 
     */
    static private $http_status = [
        404 => [
            'status' => 'Not found.',
            'message' => 'The requested URL  was not found on this server.',
            'class' => 'text-danger'
        ],
        403 => [
            'status' => 'Forbidden.',
            'message' => 'You dont have the necessary permissions to access to requested resource.',
            'class' => 'text-warning'
        ],
        401 => [
            'status' => 'Unauthorized .',
            'message' => 'You dont have any module.',
            'class' => 'text-mutted'
        ]
    ];

    /**
     * -------------------------------------------------------------------------
     * Show a error page
     * -------------------------------------------------------------------------
     * @param string $title
     * @param string $description
     */
    static function showError($title, $description)
    {

        // create logger
        $Logger = new Logger($title);
        $Logger->pushHandler(new StreamHandler(__LOG__, Logger::DEBUG));
        $Logger->pushHandler(new FirePHPHandler());
        $Logger->error($description);

        if (PHP_SAPI != 'cli') {
            include __COREFOLDER__ . 'exception/error_page.php';
            die();
        } else {
            echo "\n**************************************\n";
            echo " ERROR ";
            echo "\n**************************************\n";
            echo $title . "\n";
            echo $description . "\n";
            echo "\n**************************************\n";

            die();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Show a 404 error page
     * -------------------------------------------------------------------------
     * @param string $description
     */
    static function showHttpStatus($code, $description)
    {


        $response = self::$http_status[$code];
        $status = $response['status'];
        $code_status = $code;
        $message = $response['message'];
        $class = $response['class'];

        // create logger
        $Logger = new Logger('Error ' . $code);
        $Logger->pushHandler(new StreamHandler(__LOG__, Logger::DEBUG));
        $Logger->pushHandler(new FirePHPHandler());
        $Logger->error($description);
        include __COREFOLDER__ . 'exception/http_response.php';
        die();
    }

    /**
     * -------------------------------------------------------------------------
     * Show a exception page
     * -------------------------------------------------------------------------
     * @param string $title
     * @param string $exception
     * @param string $query
     * @param array $binds
     */
    static function ShowException($title, $exception, $die = true)
    {


        // create logger
        $Logger = new Logger($title);
        $Logger->pushHandler(new StreamHandler(__LOG__, Logger::DEBUG));
        $Logger->pushHandler(new FirePHPHandler());
        $Logger->error($exception);
        if (PHP_SAPI != 'cli') {
            include __COREFOLDER__ . 'exception/exception_page.php';
            ($die) ? die() : '';
        } else {
            echo "\n**************************************\n";
            echo " EXCEPTION ";
            echo "\n**************************************\n";
            echo $title . "\n";
            echo print_r($exception) . "\n";
            echo "\n**************************************\n";

            die();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Show a successful operation
     * -------------------------------------------------------------------------
     * @param string $title
     * @param string $description
     */
    static function ShowSuccessful($title, $description)
    {

        include __COREFOLDER__ . 'templates/successful_page.php';
        die();
    }

    /**
     * -------------------------------------------------------------------------
     * Show a successful operation
     * -------------------------------------------------------------------------
     * @param string $title
     * @param string $description
     */
    static function ShowSuccessfulRedirect($title, $description, $redirect_to)
    {

        include __COREFOLDER__ . 'exception/success_redirect.php';
        die();
    }

}
