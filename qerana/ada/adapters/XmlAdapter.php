<?php

/*
 * This file is part of ADA (abstract data access)
 * Copyright (C) 2019-20  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Ada\adapters;

use DOMDocument;

/**
 * *****************************************************************************
 * Description of XmlAdapter
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class XmlAdapter implements AdapterInterface {

    //put your code here
    private
            $_xml_path,
            $_xml_root,
            $_xml_file,
            /** @object simplexml */
            $_xml_simple,
            $_root_attributes = [],
            $_DomDocument,
            /**  query vars */
            $_result,
            $_query;

    public function __construct(string $xml_file, string $root, bool $strict = true) {


        $xml_parts = pathinfo($xml_file);

        if ($xml_parts['dirname'] === '.') {
            $tmp_path = __DATA__ . 'db/xml/structure/' . $xml_file . '.xml';
        } else {
            $tmp_path = $xml_file . '.xml';
        }
        $xml_fullpath = realpath($tmp_path);



        if (empty($xml_fullpath)) {

            // @TODO: create a new xml structure
            throw new \RuntimeException('XmlConnection, xml path' . $tmp_path . ' not found!!');
        }

        try {
            $this->_xml_path = $xml_fullpath;
            $this->_xml_file = pathinfo($xml_fullpath)['filename'];
            $this->_xml_root = $root;
            $XmlDom = new DOMDocument;
            $XmlDom->formatOutput = true;
            $XmlDom->load($xml_fullpath, LIBXML_NOBLANKS);

            if ($strict) {
                // check if is a valid xml
                if ($XmlDom->doctype->name != $xml_file AND $XmlDom->doctype->systemId != $this->_xml_file . '.dtd') {
                    throw new \RuntimeException('Invalid XML doctypem and DTD');
                }

                // validate structure
                if (!$XmlDom->validate()) {
                    throw new \Exception('Invalid structure');
                }
            }

            $this->_setRootAttributes();
            $this->_DomDocument = $XmlDom;

            // set query
            $this->_query = '(//' . $this->_xml_file . '/' . $this->_xml_root . ')';
        } catch (\Exception $ex) {
            \Qerana\Exceptions::ShowException('DomXmlAdapterConnection', $ex);
        }
    }

    /*     * *
     * -------------------------------------------------------------------------
     * set the first xml root node, and extract attributes
     * -------------------------------------------------------------------------
     */

    public function _setRootAttributes() {

        $xml = simplexml_load_file($this->_xml_path);

        $attributes = $xml->{$this->_xml_root}[0];
        $array = json_decode(json_encode($attributes), true);

        foreach ($array AS $k => $value):
            if ($k === '@attributes') {
                continue;
            }
            array_push($this->_root_attributes, $k);
        endforeach;
    }

    /**
     * -------------------------------------------------------------------------
     * Parse result, combine array root attrs with they respective value
     * -------------------------------------------------------------------------
     * @param type $r
     * @return type
     */
    public function _parseResult($r) {
        $result = [];
        foreach ($this->_root_attributes AS $attribute):

            $obj_attr = $r->getElementsByTagName($attribute)->item(0);
            if (is_object($obj_attr)) {
                $result[$attribute] = $obj_attr->nodeValue;
            }
        endforeach;

        return $result;
    }

    /**
     * Parse query conditions
     * @param array $conditions
     */
    private function _parseConditions(array $conditions = []) {
        // if isset conditions
        if (!empty($conditions)) {
            $this->_query .= '[';
            $c = 0;
            foreach ($conditions AS $field => $value):
                $c++;
                $condition_apply = ($c == 1) ? '' : ' and ';
                $this->_query .= $condition_apply . $field . '[text() = "' . $value . '"]';
            endforeach;
            $this->_query .= ']';
        }
    }

    /**
     * run query
     * @param array $conditions
     */
    private function _runQuery(array $conditions = []) {
        $this->_parseConditions($conditions);
        // run query
        $Xpath = new \DOMXPath($this->_DomDocument);
        $this->_result = $Xpath->query($this->_query);
    }

    /**
     * -------------------------------------------------------------------------
     * Finder xml query
     * ..........................................................................
     * @param array $conditions
     * @param array $options
     * @return type
     * 
     * multiple conditions example:
     * @url https://stackoverflow.com/questions/18547410/xpath-with-multiple-contains-on-different-elements
     * $Xpath->query('(//modules/module)[style[text()="sbadmin2"] and type[text()="qerapp"] ]');
     */
    public function find(array $conditions = [], array $options = []) {

        $this->_runQuery($conditions);
        $array_result = [];

        foreach ($this->_result AS $r):
            $array_result[] = $this->_parseResult($r);
        endforeach;

        // check fetch mode
        if (isset($options['fetch']) AND $options['fetch'] === 'one') {
            return (isset($array_result[0])) ? $array_result[0] : false;
        } else {
            return $array_result;
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Insert data into xml table
     * -------------------------------------------------------------------------
     * @param array $data
     */
    public function insert(array $data): int {

        //$auto_increment =  $this->_DomDocument->documentElement->lastChild;
        //$last = $this->_DomDocument->documentElement->lastChild;
        $auto_increment = rand();

        $newnode = $this->_DomDocument->createElement($this->_xml_root);
        $this->_DomDocument->documentElement->appendChild($newnode);

        // create autoincrement id
        $ide = $this->_DomDocument->createElement('id_' . $this->_xml_root, $auto_increment);
        $newnode->appendChild($ide);

        foreach ($data AS $k => $value):

            // if key is equal to content the create cdata section
            if ($k == 'html_content') {
                $html_content = $this->_DomDocument->createElement('html_content');
                $html_content->appendChild($this->_DomDocument->createCDATASection($value));
                $newnode->appendChild($html_content);
            } else {
                $node = $this->_DomDocument->createElement($k, $value);
                $newnode->appendChild($node);
            }


        endforeach;

        $this->_DomDocument->save($this->_xml_path);

        return $auto_increment;
    }

    public function setResource(string $resource): void {
        
    }

    /**
     * UPDATE using xml load file
     * @param array $data
     * @param array $conditions
     */
    public function update1(array $data, array $conditions = array()) {

        $XmlObject = simplexml_load_file($this->_xml_path);
        $root_elements = $XmlObject->xpath($this->_xml_root);

        $attribute = array_keys($conditions);
        $value = array_values($conditions);

        foreach ($root_elements AS $k => $Element):

            if ($Element->{$attribute[0]} == $value[0]) {

                foreach ($data AS $field => $value):
                    $Element->{$field} = $value;
                endforeach;
            }
        endforeach;

        $XmlObject->asXML($this->_xml_path);
    }

    /**
     * Update xml node, USING domxpath
     * @param array $data
     * @param array $conditions
     */
    public function update(array $data, array $conditions = array()) {
        // run query
        $this->_runQuery($conditions);

        foreach ($this->_result AS $Node):
            foreach ($data AS $field => $value):
                $Node->getElementsByTagName($field)->item(0)->nodeValue = filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);
            endforeach;

        endforeach;
        $this->_DomDocument->save($this->_xml_path);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete a xml element
     * -------------------------------------------------------------------------
     * @param array $conditions
     */
    public function delete(array $conditions) {
        $XmlObject = simplexml_load_file($this->_xml_path);
        $root_elements = $XmlObject->xpath($this->_xml_root);

        $attribute = array_keys($conditions);
        $value = array_values($conditions);


        foreach ($root_elements AS $k => $Element):

            if ($Element->{$attribute[0]} == $value[0]) {

                unset($XmlObject->{$this->_xml_root}[$k]);
            }
        endforeach;

        $XmlObject->asXML($this->_xml_path);
    }

}
