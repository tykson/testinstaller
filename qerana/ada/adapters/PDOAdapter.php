<?php

/*
 * This file is part of ADA (abstract data access)
 * Copyright (C) 2019  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Ada\adapters;

/*
  |--------------------------------------------------------------------------
  | PDO ADAPTER
  |--------------------------------------------------------------------------
  |
  | NAIVE DAL
  |
 */

class PDOAdapter implements AdapterInterface
{

    private
    /**
     * @object, db connection instance 
     */
            $_db_connection,
            /**
             * @string PDO STATEMENT
             */
            $_statement,
            /**
             * @string , mode to fetch PDO
             */
            $_fetch_mode = \PDO::FETCH_ASSOC,
            /**
             *  @string , name off table
             */
            $_table_name,
            /**
             * @array , primary keys of the table
             */
            $_pks = [],
            /**
             * @string , query for finders
             */
            $_query_find,
            /**
             * @array, binds for query finder 
             */
            $_find_binders = [];
    public
            $_master_query;

    public function __construct(object $DB,$table_name = null)
    {
        $this->_db_connection = $DB;
        if(!is_null($table_name)){
            $this->setResource($table_name);
        }
        
    }

    /*
      |*************************************************************************
      | BASICS
      |*************************************************************************
     */

    /**
     * -------------------------------------------------------------------------
     * Set table name
     * -------------------------------------------------------------------------
     * @param string $table_name
     */
    public function setResource(string $table_name): void
    {
        $this->_table_name = $table_name;
    }

    /**
     * -------------------------------------------------------------------------
     * Set table primary keys
     * -------------------------------------------------------------------------
     * @param array $pks
     */
    public function setPks(array $pks = []): void
    {
        $this->_pks = $pks;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all keys
     * -------------------------------------------------------------------------
     * @return array
     */
    public function getPks(): array
    {
        return $this->_pks;
    }

    /**
     * -------------------------------------------------------------------------
     * Get the table name
     * -------------------------------------------------------------------------
     * @return string
     */
    public function getTable(): string
    {
        if (empty($this->_table_name)) {
            throw new \RuntimeException('No table was defined.');
        }
        return $this->_table_name;
    }

    /*
      |*************************************************************************
      | PDO METHODS
      |*************************************************************************
     */

    /**
     * -------------------------------------------------------------------------
     * Get PDO object
     * -------------------------------------------------------------------------
     * @return object
     * @throws \PDOException
     */
    public function getStatement():object
    {
        if ($this->_statement === null) {
            throw new \PDOException('No PDO Statetment object');
        }
        
        return $this->_statement;
    }

    /**
     * -------------------------------------------------------------------------
     * Prepare PDO object to execute
     * -------------------------------------------------------------------------
     * @param string $sql , the sql query
     * @param array $options 
     * @return $this object
     * @throws \RuntimeException
     */
    public function prepare(string $sql, array $options = []):object
    {
        try {
            $this->_statement = $this->_db_connection->prepare($sql, $options);
            return $this;
        } catch (\PDOException $ex) {
            throw new \RuntimeException($ex->getMessage().'.SQL:'.$sql);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Execute pdo statetment
     * -------------------------------------------------------------------------
     * @param array $binds
     * @return $this object
     * @throws \RuntimeException
     */
    public function execute(array $binds = [])
    {
        try {
            $this->getStatement()->execute($binds);
            return $this;
        } catch (\PDOException $ex) {
            throw new \RuntimeException($ex->getMessage());
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Fetch result
     * -------------------------------------------------------------------------
     * @param type $fetch_mode
     * @param type $orientation
     * @param type $offset
     * @return type
     * @throws \RuntimeException
     */
    
    
    
    public function fetch($fetch_mode = null, $orientation = null, $offset = null)
    {
        $fetch_style = (is_null($fetch_mode)) ? $this->_fetch_mode : $fetch_mode;

        try {
            return $this->getStatement()->fetch($fetch_style, $orientation, $offset);
        } catch (\PDOException $ex) {
            throw new \RuntimeException($ex->getMessage());
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Fetch all results
     * -------------------------------------------------------------------------
     * @param type $fetch_mode
     * @param type $column
     * @return type
     * @throws \RuntimeException
     */
    public function fetchAll($fetch_mode = null, $column = 0): array
    {
        $fetch_style = $fetch_style = (is_null($fetch_mode)) ? $this->_fetch_mode : $fetch_mode;

        try {

            return ($fetch_style === \PDO::FETCH_COLUMN) ?
                    $this->getStatement()->fetchAll($fetch_style, $column) :
                    $this->getStatement()->fetchAll($fetch_style);
        } catch (\PDOException $ex) {
            throw new \RuntimeException($ex->getMessage());
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Get last inserted ID
     * -------------------------------------------------------------------------
     * @param type $name
     * @return type
     */
    public function getLastInsertId($name = null)
    {
        return $this->_db_connection->lastInsertId($name);
    }

    /*
      |*************************************************************************
      | READ OPERATIONS
      |*************************************************************************
     */

    /**
     * -------------------------------------------------------------------------
     * Execute a SQL query string
     * -------------------------------------------------------------------------
     * @param string $query , the sql query
     * @param array $binds , params bindings
     * 
     * @return array data
     */
    public function selectByQuery(string $query, array $binds = [], string $mode = 'all'):array
    {

        $this->prepare($query)->execute($binds);
        $retval = ($mode == 'all') ? $this->fetchAll() : $this->fetch();
        
        if(empty($retval)){
            return [];
        }else{
            return $retval;
        }
        
    }

    /**
     * -------------------------------------------------------------------------
     * Find something
     * -------------------------------------------------------------------------

     * @param array $conditions
     * @param array $options multiple options to apply in a query statement
     * 
     * [
     *  fetch => (all,one) , all = fetch all 
     *  orderby = order to apply ie: ORDER BY id DESC;
     *  limit = limit to apply
     *  
     * ]
     * @param type $fields

     */
    public function find(array $conditions = [], array $options = [], $fields = '*')
    {

        if (!isset($this->_master_query) AND empty($this->_master_query)) {
            $this->_query_find = ' SELECT ' . $fields . ' FROM ' . $this->getTable();
        } else {
            $this->_query_find = $this->_master_query;
        }

        // build conditions array based 
        $operator = (key_exists('operator', $options)) ? $options['operator'] : '=';
        $this->_parseConditions($conditions, $operator);

        $this->_query_find .= (key_exists('groupby', $options)) ? ' GROUP BY ' . $options['groupby'] : '';
        $this->_query_find .= (key_exists('orderby', $options)) ? ' ORDER BY ' . $options['orderby'] : '';
        $this->_query_find .= (key_exists('limit', $options)) ? ' LIMIT ' . $options['limit'] : '';

        $this->prepare($this->_query_find)->execute($this->_find_binders);
        $mode = (isset($options['fetch'])) ? $options['fetch'] : 'all';
        return ($mode == 'all') ? $this->fetchAll() : $this->fetch();
    }

    
    /**
     * -------------------------------------------------------------------------
     * Get one result, uses find
     * -------------------------------------------------------------------------
     * @param array $conditions
     * @param array $options
     * @param type $fields
     * @return type
     */
    public function findOne(array $conditions = [],array $options = [],$fields = '*'){
        
        $options['fetch']= 'one';
        return $this->find($conditions,$options,$fields);
    }
    
    /**
     * -------------------------------------------------------------------------
     * Parse conditions to find querys
     * -------------------------------------------------------------------------
     * @param type $conditions
     */
    private function _parseConditions(array $conditions = [], $operator = null)
    {
        $c = 0;
        foreach ($conditions AS $field => $search) :
            $c++;
            $boolean_operator = ($c == 1) ? ' WHERE ' : ' AND ';

            // if $search is a array
            if (is_array($search)) {

                $cond_operator = (isset($search['operator'])) ? $search['operator'] : '=';
                unset($search['operator']);

                $draw = strpos($cond_operator, 'raw=');

                // if is not a RAW QUERY
                if ($draw === false) {

                    foreach ($search AS $k => $v):
                        $this->_buildQueryAndBinds($k, $v, [
                            'boolean_operator' => $boolean_operator,
                            'operator' => $cond_operator
                        ]);
                    endforeach;
                    
                }
                // draw raw query
                else {
                    $this->_query_find .= $boolean_operator . ' ' . substr($cond_operator, 4);
                }
            } else {
                $this->_buildQueryAndBinds($field, $search, [
                    'boolean_operator' => $boolean_operator,
                    'operator' => $operator
                ]);
            }

        endforeach;
    }

    /**
     * -------------------------------------------------------------------------
     * Build a query finder and set the binds
     * -------------------------------------------------------------------------
     * @param type $field
     * @param type $value
     * @param array $options
     */
    private function _buildQueryAndBinds($field, $value, array $options = [])
    {
        $array_field = explode('.', $field);
        $field_without_alias = end($array_field);

        $this->_query_find .= $options['boolean_operator'] . $field . ' ' . $options['operator'] . ' :' . $field_without_alias . '';
        $this->_find_binders[':' . $field_without_alias] = $value;
    }

    /*
      |*************************************************************************
      | CUD operations
      |*************************************************************************
      |
      | Create,Update,Delete
      |*************************************************************************
     */

    /**
     * -------------------------------------------------------------------------
     * Insert data
     * -------------------------------------------------------------------------
     * @param array $data
     * @return int
     */
    public function insert(array $data, $table = ''): int
    {

        $table_name = (empty($table)) ? $this->getTable() : $table;

        $cols = implode(', ', array_keys($data));
        $values = implode(', :', array_keys($data));
        foreach ($data as $col => $value) {
            unset($data[$col]);
            $data[":" . $col] = $value;
        }

        $query_insert = 'INSERT INTO ' . $table_name . ' (' . $cols . ') VALUES (:' . $values . ')';

        return $this->prepare($query_insert)->execute($data)->getLastInsertId();
    }

    /**
     * --------------------------------------------------------------------------
     * Update a record
     * --------------------------------------------------------------------------
     * @param array $data , associative array field => 'value'
     * @param array $conditions , where conditions
     * @param type $table , optional , the table
     * @return type
     */
    public function update(array $data, array $conditions = [], $table = '')
    {

        $table_name = (empty($table)) ? $this->getTable() : $table;

        $binds = [];
        foreach ($data AS $col => $value):

            unset($data[$col]);
            $binds[':' . $col] = $value;
            $data[] = $col . '= :' . $col;
        endforeach;

        $query_update = 'UPDATE ' . $table_name . ' SET ' . implode(', ', $data);

        // build conditions array based 
        if ($conditions):
            $c = 0;
            foreach ($conditions AS $field => $search) :
                $c++;
                $boolean_operator = ($c == 1) ? ' WHERE ' : ' AND ';
                $query_update .= $boolean_operator . $field . ' = :' . $field . '';
                $binds[':' . $field] = $search;
            endforeach;
        endif;
        return $this->prepare($query_update)->execute($binds);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete a record
     * -------------------------------------------------------------------------
     * @param array $conditions
     * @param type $table
     * @return type
     */
    public function delete(array $conditions, $table = '')
    {
        $table_name = (empty($table)) ? $this->getTable() : $table;
        $query_delete = ' DELETE FROM ' . $table_name . ' ';

        // build conditions array based 
        $c = 0;
        foreach ($conditions AS $col => $value) :
            $c++;
            unset($conditions[$col]);
            $boolean_operator = ($c == 1) ? ' WHERE ' : ' AND ';
            $query_delete .= $boolean_operator . $col . ' = :' . $col . '';
            $conditions[':' . $col] = $value;
        endforeach;

        return $this->prepare($query_delete)->execute($conditions);
    }

}
