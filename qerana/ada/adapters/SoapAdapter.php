<?php

/*
 * This file is part of QeranaProject
 * Copyright (C) 2017-2018  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
  |--------------------------------------------------------------------------
  | SOAP ADAPTER
  |--------------------------------------------------------------------------
  |
  | manage soap conections
  |
 */

namespace Qerana\ada\adapters;

use Qerana\ada\interfaces\AdapterInterface;

class SoapAdapter implements AdapterInterface
{
    
    public 
            $client;


    private 
            
            
            $_location,
            
            $_uri;
    
    public function __construct(string $location, string $uri)
    {
        $this->client = new \SoapClient(null,['location'=> $location,'uri'=> $uri]);
    }
    
 
    public function find(){
        
    }
    
    
        /**
     * -------------------------------------------------------------------------
     * Magic method- experimental to call protected method from a object
     * -------------------------------------------------------------------------
     */
    public function __call($function, $arguments = '')
    {
        $args = (!empty($arguments)) ? implode(', ', $arguments) : '';
        return $this->client->$function($args);
    }

}
