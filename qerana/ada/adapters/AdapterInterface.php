<?php

/*
 *  * This file is part of ADA (abstract data access)
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Ada\adapters;

/**
 * *****************************************************************************
 * Description of AdapterInterface
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
interface AdapterInterface
{
    
    public function setResource(string $resource):void;
    
    public function find(array $conditions = [],array $options = []);
    
    public function insert(array $data):int;
    
    public function update(array $data, array $conditions = []);
    
    public function delete(array $conditions);
}
