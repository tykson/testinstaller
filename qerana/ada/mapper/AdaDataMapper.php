<?php

/*
 * This file is part of ADA (abstract data access)
 * Copyright (C) 2019  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Ada\mapper;

use Ada\adapters\AdapterInterface,
QException\Exceptions;

/**
 * -----------------------------------------------------------------------------
 * Abstract data mapper
 * -----------------------------------------------------------------------------
 */
abstract class AdaDataMapper
{

    protected
            $_Adapter,
            $_EntityTable;

    public function __construct(AdapterInterface $Adapter)
    {
        $this->_Adapter = $Adapter;
    }


    /**
     * -------------------------------------------------------------------------
     * Find and return one 
     * -------------------------------------------------------------------------
     * @param array $conditions
     * @param array $options
     * @return type
     */
    public function findOne(array $conditions = [], array $options = [])
    {
        $options['fetch'] = 'one';

        $row = $this->_Adapter->find($conditions, $options);

        if (!$row) {

            return null;
        } else {
            return $this->createEntity($row);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Find all 
     * -------------------------------------------------------------------------
     * @param array $conditions
     * @return array entity collection
     */
    public function findAll(array $conditions = [], array $options = [])
    {
        $entities = [];

        //check if fetch => one options was defined
        if(isset($options['fetch']) AND $options['fetch'] == 'one'){
            
            return $this->findOne($conditions);
        }
        
        $rows = $this->_Adapter->find($conditions, $options);

        // if fetch results , create entity object for each row, and 
        // store it in entity array
        if ($rows) {

            foreach ($rows as $row):
                $entities[] = $this->createEntity($row);
            endforeach;
        }
        return $entities;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all object properties and return an array with these keys and values
     * -------------------------------------------------------------------------
     * @param object $Object
     * @return array
     */
    public function getDataObject(object $Object,bool $setted = true): array
    {

        // first get all atributes
        $object_properties = (array) $Object;
        $object_datavalues = [];

        foreach ($object_properties AS $k => $v):
            $key = trim(str_replace(['*'], '', $k));
        
            if($setted){
                ($key != '_id' AND substr($key, 0, 1) == '_' AND $v != '') ? $object_datavalues[substr($key, 1)] = $v : '';
            }else{
                 ($key != '_id' AND substr($key, 0, 1) == '_') ? $object_datavalues[substr($key, 1)] = $v : '';
            }
            
        endforeach;

        (empty($object_datavalues)) ? Exceptions::showError('NoFormData', 'EmptyFormData') : '';

        return $object_datavalues;
    }

    /**
     * -------------------------------------------------------------------------
     * Create a entity, delegated to concrete mapper
     * -------------------------------------------------------------------------
     */
    abstract protected function createEntity(array $row);
}
