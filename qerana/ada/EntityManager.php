<?php

/*
 * This file is part of QeranaProject
 * Copyright (C) 2017-2018  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
  |--------------------------------------------------------------------------
  | ABSTRACT ENTITY ADA
  |--------------------------------------------------------------------------
  |
  | Mutators, accessors, magic methods to call specified method entity
  |
 */

namespace Ada;

abstract class EntityManager
{

    /**
     * -------------------------------------------------------------------------
     * Setter Mutator
     * -------------------------------------------------------------------------
     * @param type $name
     * @param type $value
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function __set($name, $value)
    {

        $field = '_' . strtolower($name);

        if (!property_exists($this, $field)) {
            throw new \InvalidArgumentException('Property <b>' . $field . '</b> dont exist on setProperty ');
        }

        $mutator = 'set_' . strtolower($name);

        // if exist the setter
        if (method_exists($this, $mutator) AND is_callable([$this, $mutator])) {
            $this->$mutator($value);
        }
        // if not exists, then assign property directly
        else {
            $this->$field = $value;
        }



        return $this;
    }

    /**
     * -------------------------------------------------------------------------
     * Entity accessor
     * -------------------------------------------------------------------------
     * @param type $name
     * @return type
     * @throws \InvalidArgumentException
     */
    public function __get($name)
    {
        if (!is_object($name)) {
            $prop = '_' . strtolower($name);


            if (!property_exists($this, $prop)) {

                throw new \InvalidArgumentException('Property <b>' . $prop . '</b> dont exist on "getProperty" ');
            }

            // return $this->$prop;
            $accesor = 'get_' . strtolower($name);

            // if exist the setter
            if (method_exists($this, $accesor) AND is_callable([$this, $accesor])) {
                $ret = $this->$accesor();
            } else {
                $ret = $this->$prop;
            }

            return $ret;
        } else {
            return $name;
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Populate object properties from a data source
     * -------------------------------------------------------------------------
     * @param array $data
     */
    public function populate(array $data)
    {

        foreach ($data AS $k => $v) {
            $this->__set($k, $v);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Allow to object to be a json serializable, alway run a getter for each property
     * -------------------------------------------------------------------------
     * @return type
     */
    public function jsonSerialize()
    {
        $properties = get_object_vars($this);
        
        $json_result = [];

        foreach ($properties AS $k => $v):
            
            $pos = strpos($k, '_');
            
            if (is_numeric($pos) AND $pos == 0) {
                $key = substr($k, 1);
                $json_result[$key] = $this->__get($key);
            } else {
                $json_result[$k] = $v;
            }
        endforeach;


        return $json_result;
    }

}
