<?php
/*
 * This file is part of ADA (abstract data access)
 * Copyright (C) 2019  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Ada;

use QException\Exceptions;

/*
  |--------------------------------------------------------------------------
  | GET SINGLETON CONECTION TO PDO A MYSQL DB
  |--------------------------------------------------------------------------
  |
 */

use PDO;

class MysqlPDO extends PDO
{

    private static $instance = null;

    public function __construct()
    {
        $config = \Qerana\Configuration::singleton();

        
        /**
         * ---------------------------------------------------------------------
         * PDO options
         * ---------------------------------------------------------------------
         */
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, // error exception mode
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'" // utf8 always
        ];

        try {
            parent::__construct('mysql:host=' . $config->get('_dbhost_') .
                    ';port=' . $config->get('_dbport_') . ';dbname=' .
                    $config->get('_dbname_'), $config->get('_dbuser_'), $config->get('_dbpass_'), $options);
        } catch (\Exception $ex) {
             Exceptions::ShowException('DataBaseConnectionFail', $ex);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Singleton
     * -------------------------------------------------------------------------
     */
    public static function singleton()
    {
        if (self::$instance == null) {

            self::$instance = new self();
        }
        
        return self::$instance;
    }

}
