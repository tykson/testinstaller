<?php

/*
 * This file is part of Qerana
 * Copyright (C) 2017-2018  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerana\core;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |--------------------------------------------------------------------------
  | SESSIONHANDLER CLASSS
  |--------------------------------------------------------------------------
  | Will stored the session values in a mysql-table, this is the implentation
  | of the session-handler
  |
 */

class QSession
{

    public function __construct()
    {
        $this->startSession();
    }

    /**
     * -------------------------------------------------------------------------
     * Start secure Session
     * -------------------------------------------------------------------------
     */
    public function startSession()
    {

        $config = \Qerana\Configuration::singleton();

        if (!isset($_SESSION)) {


            // cookies propagation only via cookies, not url
            ini_set('session.use_only_cookies', 1);

            // entropy file to generate sessions 
            ini_set('session.entropy_file', '/dev/urandom');

            // get cookies parameters
            $cookie_params = session_get_cookie_params();

            session_set_cookie_params(
                    $cookie_params['lifetime'], $cookie_params['path'], $cookie_params['domain'], $config->get('_session_https_'), $config->get('_session_http_only_')
            );

            // encrypt the sessions if is seted in keranaConf
            if (in_array($config->get('_session_hash_'), hash_algos())) {
                ini_set('session.hash_function', $config->get('_session_hash_'));
            }

            // start the secure session, and regenerate for each petition.
            session_name($config->get('_session_name_'));



            // Hash Useragent to be safe from storing malicious useragent text as session data on server
            $useragent_hash = hash('sha256', filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_SPECIAL_CHARS));

            session_start();
        }
        // Make sure we have a canary set
        if (!isset($_SESSION['canary'])) {
            // Regenerate & delete old session as well
            session_regenerate_id(true);

            $_SESSION['canary'] = [
                'birth' => time(),
                'ip' => filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_VALIDATE_IP),
                'useragent_hash' => $useragent_hash
            ];
        }

        // Regenerate session ID every 5 minutes:
        if ($_SESSION['canary']['birth'] < time() - 300) {
            // Regenerate & delete old session as well
            session_regenerate_id(true);
            $_SESSION['canary']['birth'] = time();
        }

        session_regenerate_id(true);
    }

    public function destroy($id)
    {
        echo "eliminando sesiones antiguas";
        $this->_sessions->_setIdTableValue($id, false);
        if ($this->_sessions->delete()) {
            return true;
        } else {
            return false;
        }
    }

    public function gc($maxlifetime)
    {
        $old = time() - $maxlifetime;
        if ($this->_sessions->deleteOldSession($old)) {
            return true;
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Purge all sessions vars
     * -------------------------------------------------------------------------
     */
    public function cleanSession()
    {

        $_SESSION = [];
        $params = session_get_cookie_params();

        // delete current cookie
        setcookie(
                session_name(), '', time() - 42000, $params['path'], $params['domain'], $params['secure'], $params['httponly']
        );
        // destruye la sesion
        session_destroy();
    }

}
