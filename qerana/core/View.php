<?php

/*
 * This file is part of Qerana
 * Copyright (C) 2017-2018  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerana\core;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');

/*
  |--------------------------------------------------------------------------
  | VIEW CLASS
  |--------------------------------------------------------------------------
  | load views, simply
  |
 */

class View
{

    private static
    /** @boolean is_ajax request? */
            $_is_ajax;
    public static
            $model;

    /**
     * -------------------------------------------------------------------------
     * Show a view template
     * -------------------------------------------------------------------------
     * @param string $template , html template
     * @param array $vars , parameters to pass to a view
     * @param boolean $save , if you want to sotre the rendered view in a variable
     */
    public static function showView($template, $vars = '', bool $save = false)
    {

        self::_checkAjaxPetition();

        
        $module = $_SESSION['module_name'];

        // custom layout
        $custom_layout = ($_SESSION['module_layout'] != '') ? '/' . $_SESSION['module_layout'] : '';

        // plugins to load
        $plugins = (isset($vars['Plugins'])) ? $vars['Plugins'] : [];

        $template_path = $module . '/view/' . $template . '.php';
        $partial_path = ($_SESSION['module_type'] === 'qerapp') ? __QERAPPSFOLDER__ : __MODULEFOLDER__;
        $full_path_template = $partial_path . $template_path;

        $layout = (isset($_SESSION['Q_layout'])) ? $_SESSION['Q_layout'] . $custom_layout : 'default';

 
        // check if template path exists
        // if not exists, pass the template to a viewMaker to try to create it


        (!file_exists($full_path_template)) ? \QException\Exceptions::showError('View error', 'View dont exists, or is misspelled <b>' . $template_path) . '</b>' : '';

        // processs the parameters is available in the template view
        if (is_array($vars)) {
            foreach ($vars as $key => $valor) {
                $$key = $valor;
            }
        }

        // if $save is true, create a buffer and store tne entire template rendered
        // in a variable, & return this variable
        if ($save) {
            ob_start();
            include($full_path_template );
            $var_view = ob_get_contents();
            ob_end_clean();
            return $var_view;
        }

        // check if layout exists

        if (!realpath(__ROOTFOLDER__ . '_layouts_/' . $layout)) {
           \QException\Exceptions::showError('ViewError', 'The layout dont exists :layouts/' . $layout);
        }


        // if want to load the html header
        (!self::$_is_ajax) ? require_once(__ROOTFOLDER__ . '_layouts_/' . $layout . '/_header.php') : '';

        // include the template file
        include($full_path_template);

        // if want to load the footer

        
        $footer_path = realpath(__ROOTFOLDER__ . '_layouts_/' . $layout . '/_footer.php');
        
        

        (!self::$_is_ajax) ? require_once($footer_path) : require_once(__ROOTFOLDER__ . '/_layouts_/_plugins.php');
    }


    /**
     * -------------------------------------------------------------------------
     * Show the forgotpassword page
     * -------------------------------------------------------------------------
     */
    public static function showRecoveryForm()
    {
        include(__ROOTFOLDER__ . '/_layouts_/login/recovery_account.php');
    }

    /**
     * -------------------------------------------------------------------------
     * Show the user activation page
     * -------------------------------------------------------------------------
     */
    public static function showUserActivationPage()
    {
        include(__ROOTFOLDER__ . '/_layouts_/login/user_activation.php');
    }

    /**
     * -------------------------------------------------------------------------
     * Show the user reacactivation page
     * -------------------------------------------------------------------------
     */
    public static function showUserReActivationPage()
    {
        include(__ROOTFOLDER__ . '/_layouts_/login/user_reactivation.php');
    }

    /**
     * -------------------------------------------------------------------------
     * Load form with CSRF token security
     * -------------------------------------------------------------------------
     * @param mixed $template
     * @param array $vars
     * @param object $model , object of model to create a html form
     */
    public static function showForm($template, array $vars = [])
    {
        //$token_string = \kerana\Security::csrfGetTokenId();
        $token_value = \helpers\Security::csrfGetTokenValue();
        // add the token to "params" array
        $vars['kerana_token'] = '<input type="hidden" name="_kerana_token_" value="' . $token_value . '">';

        self::showView($template, $vars, false);
    }

    /**
     * -------------------------------------------------------------------------
     * Check if is a jax petition
     * -------------------------------------------------------------------------
     */
    private static function _checkAjaxPetition()
    {

        $server_request = FILTER_INPUT(INPUT_SERVER, "HTTP_X_REQUESTED_WITH");
        self::$_is_ajax = (isset($server_request) AND $server_request === 'XMLHttpRequest') ? true : false;
        
    }

}
