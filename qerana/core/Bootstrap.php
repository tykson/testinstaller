<?php

/*
 * This file is part of keranaProject
 * Copyright (C) 2017-2018  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerana\core;

use Monolog\Logger,
    Monolog\Handler\StreamHandler,
    Monolog\Handler\FirePHPHandler,
    Monolog\ErrorHandler;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |--------------------------------------------------------------------------
  | Qerana boot
  |--------------------------------------------------------------------------
  | @author = diem@rc;
  | @date 24/08/2017
  | Front controller implementation
 */

class Bootstrap {

    private
    /** @var mixed, full namespace  */
            $_namespace,
            /** @var mixed, module to load */
            $_module,
            /** @var mixed, controller to use */
            $_controller,
            /** @var mixed, object controller */
            $_object_controller,
            /** @var mixed, method to call */
            $_action,
            /** @var array, url parameters passed via GET */
            $_parameters,
            /** @var string, url string */
            $_url_string;
    public
            $config;

    /**
     * -------------------------------------------------------------------------
     * Start the application
     * -------------------------------------------------------------------------
     * @return boolean
     */
    public function __construct() {


        // create logger
        $Log = new Logger('qfw');
        $Log->pushHandler(new StreamHandler(__LOG__, Logger::DEBUG));
        $Log->pushHandler(new FirePHPHandler());

        $handler = new ErrorHandler($Log);
        $handler->registerErrorHandler([], false);
        $handler->registerExceptionHandler();
        $handler->registerFatalHandler();

        //load qerana confguration file.
        $this->config = \Qerana\Configuration::singleton();

        // load the configuration file depending on environment value constant
        require_once(__ROOTFOLDER__ . 'config/conf.' . __ENVIRONMENT__ . '.php');
        require_once(__ROOTFOLDER__ . 'config/db_conf.' . __ENVIRONMENT__ . '.php');


        // segment url, and store parameters
        $this->buildPetition();


        // object controller is created now
        $this->_object_controller = new $this->_namespace;


        // if exists some parameters will stored in a array 
        // ej: indexController->index($i,$y)
        if (!empty($this->_parameters)) {

            // call the method and pass the parameters
            call_user_func_array([$this->_object_controller, $this->_action], $this->_parameters);
        } else {

            // if not parameters , the call the method

            $this->_object_controller->{$this->_action}();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Build the module,controller,action,parameters from a friendly url
     * -------------------------------------------------------------------------
     * ej: http://keranaproject/comercial/empresa/editar/10054
     * modulo = comercial
     * controlador = empresa
     * action = editar
     * par1 = 10054
     * -------------------------------------------------------------------------
     */
    private function buildPetition() {

        // if is CLI
        if (defined('__ISCLI__')) {
            $this->_buildCliRequest();
        }

        // http mode
        else {

            $this->_buildHttpRequest();
        }

        try {
            // check if is callable the namespace
            if (is_callable(array($this->_namespace, $this->_action)) == false) {

                $descripcion = "$this->_namespace not found!";
                \QException\Exceptions::showHttpStatus(404, $descripcion);
                return false;
            }
        } catch (\Throwable $ex) {

            \QException\Exceptions::ShowException('CriticalError', $ex);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Build CLI request
     * -------------------------------------------------------------------------
     */
    private function _buildCliRequest() {
        $arg = $_SERVER['argv']; //$_SERVER['argv']);
        $this->_module = $arg[1];
        $this->_controller = ucwords($arg[2]);
        $this->_action = $arg[3];
        unset($arg[0], $arg[1], $arg[2], $arg[3]);

        $this->_parameters = $arg;

        if ($this->_module == 'cliqer') {
            $base_mod = realpath(__COREFOLDER__ . '/cliqer');
            $namespace = '\\Cliqer\\' . $this->_controller;
        } else {
            $base_mod = __APP__;
            $namespace = '\\' . $base_mod . '\\' . $this->_module . '\\controller\\' . $this->_controller . 'Controller';
        }

        $this->_namespace = $namespace;
    }

    /**
     * -------------------------------------------------------------------------
     * build http response
     * -------------------------------------------------------------------------
     */
    private function _buildHttpRequest() {


        $Module = \helpers\Url::getModuleInfo();

        // determine if is a qerapp module
        $app = ($Module->type === 'qerapp') ? __QERAPP__ : __APP__;

        // create a session with modukle object
        $Session = new QSession;
        $_SESSION['module_type'] = $Module->type;
        $_SESSION['module_name'] = $Module->name;
        $_SESSION['module_layout'] = $Module->layout;

        // get url parts
        $controller = \helpers\Url::getController();
        $action = \helpers\Url::getAction();


        $this->_module = $Module->name;
        $this->_controller = ($this->_module == 'qcontent' AND empty($controller)) ? 'Page' : $controller;
        $this->_action = ($this->_module == 'qcontent' AND empty($action)) ? 'view' : $action;
        $this->_parameters = ($this->_module == 'qcontent' AND empty($action)) ? [\helpers\Url::getModule()] : \helpers\Url::getParameters();
        // set the url string
        $this->_url_string = $this->_module . '/' . $this->_controller . '/' . $this->_action;
//
//        echo '<pre>';
//        print_r($this->_parameters);
//        die();
//        die();

        // full namespace
        $this->_namespace = '\\' . $app . '\\' . $this->_module . '\\controller\\' . $this->_controller . 'Controller';



        // check login if necesary
        // iif mofdule has a private access and namespace has not control exceptions
        if (($Module->access === 'private') AND ( !in_array($this->_namespace, $this->config->get('_access_exceptions_')))) {
            $this->_checkAccessPetition();

            // check if can pass if acl verification is on
            if ($this->config->get('_acl_active_') == 1) {
                $AclService = new \Qerapp\qaccess\model\acl\AclService();
                $AclService->checkAclUser($this->_url_string);
            }
        }
        //else {
//            // public modules uses a default layout
//
//            $_SESSION['Q_layout'] = $Module->style;
//        }
    }

    /**
     * -------------------------------------------------------------------------
     * Check Access Petition
     * -------------------------------------------------------------------------
     * Check if the module requested is not a public module, specified in conf file,
     * If false, check user autentification.
     * If user user is autentificated, then check ACL if this is activated in conf file
     */
    private function _checkAccessPetition() {

        $Login = New \Qerapp\qaccess\model\LoginService();
        $Login->check();
    }

}
