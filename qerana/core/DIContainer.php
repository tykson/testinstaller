<?php

declare(strict_types = 1);
/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerana\core;

/**
 * *****************************************************************************
 * Dependency Injection Container
 * *****************************************************************************
 *
 * *****************************************************************************
 */
class DIContainer
{

    private
            // array to map interface to respective class
            $_mapping_interface = [];

   
    protected 
            // dependencies instances
            $instances = [];

    public function __construct(array $mapping = ['Mapper' => '', 'Interface' => 'Mapper'])
    {
        $this->_mapping_interface = $mapping;
        $this->_path = realpath(__APPFOLDER__);
    }
    
    

    /**
     * -------------------------------------------------------------------------
     * Register the object instances
     * -------------------------------------------------------------------------
     * @param      $abstract
     * @param null $concrete
     */
    public function set($abstract, $concrete = NULL)
    {
        if ($concrete === NULL) {
            $concrete = $abstract;
        }
        $this->instances[$abstract] = $concrete;
    }

    /**
     * -------------------------------------------------------------------------
     * Get instance dependency objects
     * -------------------------------------------------------------------------
     * @param       $abstract
     * @param array $parameters
     *
     * @return mixed|null|object
     * @throws Exception
     */
    public function get($abstract, $parameters = [])
    {

        // if we don't have it, just register it
        if (!isset($this->instances[$abstract])) {
            $this->set($abstract);
        }
        return $this->resolve($this->instances[$abstract], $parameters);
    }

    /**
     * -------------------------------------------------------------------------
     * Resolve dependencies object
     * -------------------------------------------------------------------------
     *
     * @param $concrete
     * @param $parameters
     *
     * @return mixed|object
     * @throws Exception
     */
    public function resolve($concrete, $parameters)
    {


        if ($concrete instanceof Closure) {
            return $concrete($this, $parameters);
        }
        
        // reflector
        $Reflector = new \ReflectionClass($concrete);

        // check if class is instantiable
        if (!$Reflector->isInstantiable()) {


            // check if class is a interface
            if ($Reflector->isInterface()) {
                
                // try to find the implementation
                $namespace = $Reflector->getNamespaceName();
                $name_class = \helpers\Utils::extractStringLastPattern($Reflector->getName(), '\\');

                $class_interface = \helpers\Utils::replaceStringPattern($name_class, $this->_mapping_interface);

                $class_to_resolve = str_replace('interfaces','mapper',$namespace) . '\\' . $class_interface;
                
//                echo 'class ='.$class_to_resolve;
//                echo '<br>concrete = '.$concrete;
//                
//                die();
                
                return $this->get($class_to_resolve, $parameters);
            }
        } 
        // get class constructor
        $constructor = $Reflector->getConstructor();


        if (is_null($constructor)) {
            // get new instance from class
            return $Reflector->newInstance();
        }
        // get constructor params
        $parameters_cons = $constructor->getParameters();
        
        // extract dependencies
        $dependencies = $this->getDependencies($parameters_cons);

        // get new instance with dependencies resolved
        return $Reflector->newInstanceArgs($dependencies);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all dependencies
     * -------------------------------------------------------------------------
     *
     * @param $parameters
     *
     * @return array
     * @throws Exception
     */
    public function getDependencies($parameters)
    {
        $dependencies = [];
        foreach ($parameters as $parameter) {
            // get the type hinted class
            $dependency = $parameter->getClass();

            if ($dependency === NULL) {
                // check if default value for a parameter is available
                if ($parameter->isDefaultValueAvailable()) {
                    // get default value of parameter
                    $dependencies[] = $parameter->getDefaultValue();
                } else {
                    throw new \Exception("Can not resolve class dependency {$parameter->name}");
                }
            } else {
                // get dependency resolved
                $dependencies[] = $this->get($dependency->name);
            }
        }
        return $dependencies;
    }

}
