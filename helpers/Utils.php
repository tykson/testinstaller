<?php

/*
 * This file is part of keranaProject
 * Copyright (C) 2017-2018  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace helpers;

class Utils
{

    public static $_json_elements;

    /**
     * -------------------------------------------------------------------------
     * clean a string
     * -------------------------------------------------------------------------
     * @param string $string
     * @return type
     */
    public static function cleanString(string $string): string
    {
        $text = strtolower($string);
        $patron = array(
            // Espacios, puntos y comas por guion
            '/[\., ]+/' => '-',
            // Vocales
            'á' => 'a',
            'é' => 'e',
            'í' => 'i',
            'ó' => 'o',
            'ú' => 'u',
            'ñ' => 'n',
                // Agregar aqui mas caracteres si es necesario
        );

        return str_replace(array_keys($patron), array_values($patron), $text);
    }

    /**
     * -------------------------------------------------------------------------
     * clean a string match pattern
     * -------------------------------------------------------------------------
     * @param string $string
     * @return type
     */
    public static function replaceStringPattern(string $string, array $pattern): string
    {
        $text = $string;
        return str_replace(array_keys($pattern), array_values($pattern), $text);
    }

    /**
     * -------------------------------------------------------------------------
     * Extract from string las match with a pattern
     * -------------------------------------------------------------------------
     * @param string $string
     * @param type $pattern
     * @return type
     */
    public static function extractStringLastPattern(string $string, $pattern)
    {
        $a = explode(strrev($pattern), strrev($string), 2);
        return strrev($a[0]);
    }

    /**
     * -------------------------------------------------------------------------
     * Parse array to json notation
     * -------------------------------------------------------------------------
     * parse a array in json 
     * @param array $array
     */
    public static function parseToJson($array)
    {

        $json_response = [];
        $is_a_array = (is_array($array)) ? true : false;
        $n_elements = ($is_a_array) ? count($array) : 1;


        if ($is_a_array) {
            $records = [];
            foreach ($array AS $k => $v):

                array_push($records, self::convertObjectoArray($v));
            endforeach;


            if ($n_elements > 0) {
                $json_response['num_reg'] = $n_elements;
                $json_response['exists'] = true;

                $json_response['Result'] = $records;
            } else {
                $json_response['exists'] = false;
            }
        } else if (is_object($array)) {
            $json_response['num_reg'] = 1;
            $json_response['exists'] = true;
            $array_json = self::convertObjectoArray($array);



            foreach ($array_json AS $k => $v):
                $json_response['Result'][$k] = $v;
            endforeach;
        }else {
            $json_response['exists'] = false;
        }


        echo json_encode($json_response);
    }

    /**
     * -------------------------------------------------------------------------
     * convert object to array
     * -------------------------------------------------------------------------
     * @param object $Object
     * @return type
     */
    public static function convertObjectoArray(object $Object, $sub = true)
    {

        // to array
        $array = (array) $Object;
        $array_return = [];

        foreach ($array AS $k => $v):
            // replace all private/protected attribute mark 
            $key = trim(str_replace(['*'], '', $k));
            // substract the _ from the begin og properti

            if ($sub) {
                $key = (!is_object($v)) ? substr($key, 1) : $k;
            }


            // is $v is object
            if (is_object($v)) {
                $v = self::convertObjectoArray($v,$sub);
            }

            $array_return[$key] = $v;
        endforeach;


        return $array_return;
    }

    public static function getMemoryUsed($size)
    {
        $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }

    /**
     * -------------------------------------------------------------------------
     * Get Server memory used
     * -------------------------------------------------------------------------
     * @return type
     */
    public static function getServerMemoryUsage()
    {

        $free = shell_exec('free');
        $free = (string) trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $memory_usage = $mem[2] / $mem[1] * 100;

        return $memory_usage;
    }

    /**
     * -------------------------------------------------------------------------
     * Get CPU used by server
     * -------------------------------------------------------------------------
     * @return type
     */
    public static function getServerCpuUsage()
    {
        $load = sys_getloadavg();
        return $load[0];
    }

    /**
     * -------------------------------------------------------------------------
     * Check is $email are valid and check mx records from emal domain
     * -------------------------------------------------------------------------
     * @param type $email
     * @throws \InvalidArgumentException
     */
    public static function checkEmail($email)
    {

        if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
            throw new \InvalidArgumentException($email . ' is not valid email!!');
        } else {
            // check email domain
            $domain = explode('@', $email);

            if (!checkdnsrr($domain[1])) {
                throw new \InvalidArgumentException('EmailValidator: ' . $email
                . ' domain mx records fail');
            }
        }
        return trim($email);
    }
    
     /**
     * sum array object property, only with public properties
     * @param array $array
     * @param type $property
     * @return type
     */
    public static function sumProperty(array $array, $property)
    {

        $sum = 0;
        foreach ($array as $object) {
            $sum += isset($object->{$property}) ? $object->{$property} : 0;
        }

        return $sum;
    }

}
