<?php

/*
 * This file is part of QeranaProject
 * Copyright (C) 2017-2019  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace helpers;

use Qerapp\qbasic\model\module\ModuleService;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');

/**
 * -----------------------------------------------------------------------------
 * Class Url
 * ------------------------------------------------------------------------------
 * UrlHelper, manage url stuff, simply like that
 */
class Url {

    public static
    /** @var mixed, current module */
            $current_module,
            /** @var mixed, current controller */
            $current_controller,
            /** @var mixed, current actin */
            $current_action,
            /** @var array, get parameters url */
            $url_parameters,
            $url_string;

    /**
     * -------------------------------------------------------------------------
     * Store the diferent segment of a url
     * -------------------------------------------------------------------------
     */
    public static function setUrl() {
        $url_request = trim(filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL), '/');

        $config = \Qerana\Configuration::singleton();

        if ($url_request) {
            $url = explode('/', $url_request);

            self::$current_module = isset($url[$config->get('position_module')]) ? $url[$config->get('position_module')] : false;
            self::$current_controller = isset($url[$config->get('position_controller')]) ? $url[$config->get('position_controller')] : false;
            self::$current_action = isset($url[$config->get('position_action')]) ? $url[$config->get('position_action')] : false;

            // remove from array the other parameters to store in diferent array
            unset($url[$config->get('position_module')], $url[$config->get('position_controller')], $url[$config->get('position_action')]);

            self::$url_parameters = array_values($url);
        } else {
            self::$current_module = $config->get('default_module');
            self::$current_controller = $config->get('default_controller');
            self::$current_action = $config->get('default_action');
        }

        self::$url_string = '/' . self::$current_module . '/' . self::$current_controller . '/' . self::$current_action;
    }

    /**
     * -------------------------------------------------------------------------
     * Get current module info
     * -------------------------------------------------------------------------
     * @return type
     */
    public static function getModuleInfo() {
        self::setUrl();
        // check if module as a html content
        $is_html_content = strpos(self::$current_module, '.html');
        if ($is_html_content !== false) {
            self::$current_module = 'qcontent';
        }
        try {
            $ModuleService = new ModuleService();
            $ModuleService->module_name = self::$current_module;
            $Module = $ModuleService->getModule();

            if (!$Module) {
                \QException\Exceptions::showHttpStatus(404, 'Module ' . self::$current_module . ' dont exists');
            }

            return $Module;
        } catch (\Throwable $ex) {
            \QException\Exceptions::ShowException('ModuleService not found!!', $ex);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Get current controller 
     * -------------------------------------------------------------------------
     * @return string
     */
    public static function getController() {
        self::setUrl();

        return ucwords(self::$current_controller);
    }

    /**
     * -------------------------------------------------------------------------
     * Get current module 
     * -------------------------------------------------------------------------
     * @return string
     */
    public static function getModule() {
        self::setUrl();
        return self::$current_module;
    }

    /**
     * -------------------------------------------------------------------------
     * Get current action
     * -------------------------------------------------------------------------
     * @return string
     */
    public static function getAction() {
        self::setUrl();
        return self::$current_action;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all parameters passes by url-string
     * -------------------------------------------------------------------------
     * @return array
     */
    public static function getParameters() {
        self::setUrl();
        return self::$url_parameters;
    }

}
