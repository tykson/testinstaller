<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace helpers;

/**
 * *****************************************************************************
 * Description of Date helper
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class Date
{


    /**
     * -------------------------------------------------------------------------
     * Convert date to a format
     * -------------------------------------------------------------------------
     * @param type $date
     * @param type $format
     * @return type
     */
    public static function convertDate($date, $format = 'd-m-Y')
    {

        $Date = date_create($date);
        if(!is_object($Date)){
            throw new \InvalidArgumentException('Invalid date ');
        }
        
        return date_format($Date, $format);
    }

    /**
     * -------------------------------------------------------------------------
     * format a date to db compatibility
     * -------------------------------------------------------------------------
     * @param type $date
     * @return type
     */
    public static function toDate($date)
    {
        return self::convertDate($date, 'Y-m-d H:i:s');
    }
    
    /**
     * -------------------------------------------------------------------------
     * 
     * -------------------------------------------------------------------------
     * @param type $date
     * @return type
     */
    public static function toString($date,$format = 'd-m-Y')
    {
        return self::convertDate($date,$format);
    }

}
